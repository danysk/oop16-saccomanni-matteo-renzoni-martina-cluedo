package utilities.enumerations;

/**
 * Enumeration for all the types of cell.
 */
public enum CellType {

    /**
     * A simple cell.
     */
    SIMPLE,

    /**
     * The cell contains a door.
     */
    DOOR,

    /**
     * The cell contains a trap-door.
     */
    TRAP_DOOR;
}